import torch
import shutil
import os

def save_checkpoint(state, is_best, saving_dir, saving_prefix):
    file_path=os.path.join(saving_dir,saving_prefix+'.pth')
    file_path_best=os.path.join(saving_dir,'best_'+saving_prefix+'.pth')
    torch.save(state, file_path)
    if is_best:
        shutil.copyfile(file_path, file_path_best)

class AverageMeter(object):
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count