import torch.utils.data
from datasets.crowdDataset import CrowdDataset
from torch.utils.data import DataLoader
from utils.utils import save_checkpoint,AverageMeter
import math
from tqdm import tqdm
from utils.misc import find_latest_checkpoint
from utils.logger import get_root_logger
from datasets.crowdVideoDataset import CrowdVideoDataset,loader_wrapper
import gc
from models.frccGAN_net import FRCC,GeneratorLoss,Discriminator


class FRCCGAN_Trainer:
    def __init__(self,cfg):
        self.cfg=cfg

        self.train_path=cfg.train_path
        self.test_path=cfg.test_path
        self.scale_times=cfg.scale_times
        self.batch_size_train=cfg.batch_size_train
        self.batch_size_test=cfg.batch_size_test

        self.epochs_drop=cfg.epochs_drop
        self.drop_times=cfg.drop_times

        self.workers=cfg.workers
        self.lr=cfg.lr
        self.momentum=cfg.momentum
        self.weight_decay=cfg.weight_decay
        self.start_epoch=cfg.start_epoch
        self.resume_from=cfg.resume_from
        self.load_from=cfg.load_from
        self.epochs=cfg.epochs
        self.auto_resume=cfg.auto_resume
        self.work_dir=cfg.work_dir

        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        #############################################
        train_dataset=CrowdDataset(self.train_path)
        val_dataset=CrowdDataset(self.test_path)
        self.train_loader = DataLoader(train_dataset, batch_size=self.batch_size_train, num_workers=self.workers, shuffle=True, drop_last=True)
        self.val_loader = DataLoader(val_dataset, batch_size=self.batch_size_test,num_workers=self.workers)

        train_dataset=CrowdVideoDataset(self.train_path,train_flag=True)
        val_dataset=CrowdVideoDataset(self.test_path,train_flag=False)
        train_loader = DataLoader(train_dataset, batch_size=self.batch_size_train, num_workers=self.workers, shuffle=True, drop_last=True)
        val_loader = DataLoader(val_dataset, batch_size=self.batch_size_test,num_workers=self.workers,drop_last=True)
        #[video_seq_size,batch_size,channel_num,W,H]
        self.train_loader = loader_wrapper(train_loader)
        self.val_loader = loader_wrapper(val_loader)


        self.netG = FRCC(device=self.device).cuda()
        self.netD = Discriminator().cuda()
        self.criterion = GeneratorLoss().cuda()
        self.optimizerG = torch.optim.Adam(self.netG.parameters(), lr=self.lr, weight_decay=self.weight_decay)
        self.optimizerD = torch.optim.Adam(self.netD.parameters(), lr=self.lr, weight_decay=self.weight_decay)

        self.best_prec1=1e6
        self.logger = get_root_logger()

    def train(self,epoch):
        self.logger.info('Epoch: [{0}]\t'.format(epoch))

        lossesG = AverageMeter()
        lossesD = AverageMeter()
        self.netG.train()
        self.netD.train()
        for imgs, density_maps, dot_maps in tqdm(self.train_loader):#[5，batch_size,channel,H,W]

            fakeScrs = []
            realScrs = []
            _EstDensityMap_Single_list=[]
            _EstDotMap_Single_list=[]
            EstDensityMap_list=[]
            EstDotMap_list=[]
            Img_afterWarp_list=[]

            DLoss = 0
            # Zero-out gradients, i.e., start afresh
            self.netD.zero_grad()
            self.netG.init_hidden(self.batch_size_train, img_height=384, img_width=512)
            for img, density_map, dot_map in zip(imgs, density_maps, dot_maps):
                img = img.to(self.device)
                density_map = density_map.to(self.device)
                # dot_map = dot_map.to(device)

                _EstDensityMap_Single, _EstDotMap_Single, EstDensityMap, EstDotMap, Img_afterWarp = self.netG(img)
                realOut = self.netD(density_map).mean()
                fake_out = self.netD(EstDensityMap).mean()

                fakeScrs.append(fake_out)
                realScrs.append(realOut)
                _EstDensityMap_Single_list.append(_EstDensityMap_Single)
                _EstDotMap_Single_list.append(_EstDotMap_Single)
                EstDensityMap_list.append(EstDensityMap)
                EstDotMap_list.append(EstDotMap)
                Img_afterWarp_list.append(Img_afterWarp)

                DLoss += 1 - realOut + fake_out

            idx = 0
            GLoss = 0
            self.netG.zero_grad()
            for fake_out, _EstDensityMap_Single, _EstDotMap_Single, EstDensityMap, EstDotMap, Img_afterWarp, img, density_map, dot_map in zip(fakeScrs,_EstDensityMap_Single_list, _EstDotMap_Single_list, EstDensityMap_list, EstDotMap_list, Img_afterWarp_list, imgs, density_maps, dot_maps):
                img = img.to(self.device)
                density_map = density_map.to(self.device)
                dot_map = dot_map.to(self.device)

                GLoss += self.criterion(fake_out, _EstDensityMap_Single, _EstDotMap_Single, EstDensityMap, EstDotMap, density_map, dot_map, Img_afterWarp, img, idx)
                idx += 1

            DLoss /= len(imgs)
            DLoss.backward(retain_graph=True)
            GLoss /= len(imgs)
            GLoss.backward()
            #注意这里的顺序是有讲究的，按照这种方式来操作
            self.optimizerD.step()
            self.optimizerG.step()
            lossesD.update(DLoss.item())
            lossesG.update(GLoss.item())

            self.logger.info('Loss {lossG.val:.4f} ({lossG.avg:.4f})\t'
                             'Loss {lossD.val:.4f} ({lossD.avg:.4f})\t'.format(lossG=lossesG, lossD=lossesD))
            gc.collect()


    def validate(self,):
        self.logger.info('begin test:')
        self.netG.eval()
        mae1 = 0
        mae2 = 0
        mae3 = 0
        mae4 = 0
        with torch.no_grad():
            for imgs, density_maps, dot_maps in tqdm(self.val_loader):#[5，batch_size,channel,H,W]

                self.netG.init_hidden(self.batch_size_test, img_height=imgs.size(3), img_width=imgs.size(4))
                for img, density_map, dot_map in zip(imgs, density_maps, dot_maps):#处理一个batch的数据
                    img = img.to(self.device)
                    density_map = density_map.to(self.device)
                    dot_map = dot_map.to(self.device)
                    _EstDensityMap_Single, _EstDotMap_Single, EstDensityMap, EstDotMap, _ = self.netG(img)

                    diffs1=_EstDensityMap_Single.data.sum(dim=[1,2,3])-density_map.data.sum(dim=[1,2,3])
                    diffs2=_EstDotMap_Single.data.sum(dim=[1,2,3])-dot_map.data.sum(dim=[1,2,3])

                    diffs3=EstDensityMap.data.sum(dim=[1,2,3])-density_map.data.sum(dim=[1,2,3])
                    diffs4=EstDotMap.data.sum(dim=[1,2,3])-dot_map.data.sum(dim=[1,2,3])

                    diffs1=diffs1.cpu().numpy()
                    diffs2=diffs2.cpu().numpy()
                    diffs3=diffs3.cpu().numpy()
                    diffs4=diffs4.cpu().numpy()
                    mae1 += sum(abs(diffs1))
                    mae2 += sum(abs(diffs2))
                    mae3 += sum(abs(diffs3))
                    mae4 += sum(abs(diffs4))


        mae1 = mae1/(len(self.val_loader)*self.batch_size_test*5)
        mae2 = mae2/(len(self.val_loader)*self.batch_size_test*5)
        mae3 = mae3/(len(self.val_loader)*self.batch_size_test*5)
        mae4 = mae4/(len(self.val_loader)*self.batch_size_test*5)
        self.logger.info(' * MAE_1 {mae1:.3f} '.format(mae1=mae1))
        self.logger.info(' * MAE_2 {mae2:.3f} '.format(mae2=mae2))
        self.logger.info(' * MAE_3 {mae3:.3f} '.format(mae3=mae3))
        self.logger.info(' * MAE_4 {mae4:.3f} '.format(mae4=mae4))
        return (mae1+mae2+mae3+mae4)/4

    def resume(self):
        checkpoint = torch.load(self.resume_from)
        self.start_epoch = checkpoint['epoch']
        self.best_prec1 = checkpoint['best_prec1']
        self.netG.load_state_dict(checkpoint['state_dict_netG'])
        self.optimizerG.load_state_dict(checkpoint['optimizer_netG'])
        self.netD.load_state_dict(checkpoint['state_dict_netD'])
        self.optimizerD.load_state_dict(checkpoint['optimizer_netD'])
        self.logger.info("=> loaded checkpoint '{}' (epoch {})".format(self.resume_from, checkpoint['epoch']))

    def load_checkpoint(self):
        checkpoint = torch.load(self.load_from)
        self.best_prec1 = checkpoint['best_prec1']
        self.netG.load_state_dict(checkpoint['state_dict_netG'])
        self.optimizerG.load_state_dict(checkpoint['optimizer_netG'])
        self.netD.load_state_dict(checkpoint['state_dict_netD'])
        self.optimizerD.load_state_dict(checkpoint['optimizer_netD'])
        self.logger.info("=> loaded checkpoint '{}'".format(self.resume_from))

    def adjust_learning_rate(self,epoch):
        epochs_drop = self.epochs_drop
        self.lr = self.lr * math.pow(self.drop_times, math.floor(epoch / epochs_drop))

        for param_group in self.optimizerG.param_groups:
            param_group['lr'] = self.lr

        for param_group in self.optimizerD.param_groups:
            param_group['lr'] = self.lr

    def run(self):
        if self.resume_from =='' and self.auto_resume==True:
            resume_from = find_latest_checkpoint(self.work_dir)
            if resume_from is not None:
                self.resume_from = resume_from
        if self.resume_from!='':
            self.resume()
        elif self.load_from!='':
            self.load_checkpoint()

        for epoch in range(self.start_epoch, self.epochs):
            self.adjust_learning_rate(epoch)

            self.train(epoch)
            prec1 = self.validate()
            is_best = prec1 < self.best_prec1
            self.best_prec1 = min(prec1, self.best_prec1)
            self.logger.info(' * best MAE {mae:.3f} '.format(mae=self.best_prec1))

            save_checkpoint({
                'epoch': epoch + 1,
                'best_prec1': self.best_prec1,
                'state_dict_netG': self.netG.state_dict(),
                'optimizer_netG' : self.optimizerG.state_dict(),
                'state_dict_netD': self.netD.state_dict(),
                'optimizer_netD' : self.optimizerD.state_dict(),
            }, is_best=is_best,saving_dir= self.work_dir,saving_prefix=self.cfg.timestamp)

