import torch.nn as nn
from torch.autograd import Variable
import torch.utils.data
from datasets.crowdDataset import CrowdDataset
from torch.utils.data import DataLoader
from models.bba_net import CSRNet
from utils.utils import save_checkpoint,AverageMeter
import math
from tqdm import tqdm
from utils.misc import find_latest_checkpoint
from utils.logger import get_root_logger


class BBA_Trainer:
    def __init__(self,cfg):
        self.cfg=cfg

        self.train_path=cfg.train_path
        self.test_path=cfg.test_path
        self.scale_times=cfg.scale_times
        self.batch_size_train=cfg.batch_size_train
        self.batch_size_test=cfg.batch_size_test

        self.epochs_drop=cfg.epochs_drop
        self.drop_times=cfg.drop_times

        self.workers=cfg.workers
        self.lr=cfg.lr
        self.momentum=cfg.momentum
        self.weight_decay=cfg.weight_decay
        self.start_epoch=cfg.start_epoch
        self.resume_from=cfg.resume_from
        self.load_from=cfg.load_from
        self.epochs=cfg.epochs
        self.auto_resume=cfg.auto_resume
        self.work_dir=cfg.work_dir

        train_dataset=CrowdDataset(self.train_path)
        val_dataset=CrowdDataset(self.test_path)
        self.train_loader = DataLoader(train_dataset, batch_size=self.batch_size_train, num_workers=self.workers, shuffle=True, drop_last=True)
        self.val_loader = DataLoader(val_dataset, batch_size=self.batch_size_test,num_workers=self.workers)

        self.model = CSRNet().cuda()
        self.criterion = nn.MSELoss().cuda()
        self.optimizer = torch.optim.SGD(self.model.parameters(), self.lr, momentum=self.momentum,weight_decay=self.weight_decay)
        # optimizer = torch.optim.Adam(model.parameters(), lr=args.lr, weight_decay=1e-4)

        self.best_prec1=1e6
        self.logger = get_root_logger()
    def train(self, epoch):
        self.logger.info('Epoch: [{0}]\t'.format(epoch))
        losses = AverageMeter()
        self.model.train()
        for img, density_map, dot_map in tqdm(self.train_loader):

            img = Variable(img).cuda()
            density_map = Variable(density_map).cuda()*self.scale_times
            dot_map = Variable(dot_map).cuda()*self.scale_times

            output1,output2 = self.model(img)
            loss1 = self.criterion(output1, density_map)
            loss2 = self.criterion(output2, dot_map)
            loss=loss1+loss2
            losses.update(loss.item(), img.size(0))
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()

            self.logger.info('Loss {loss.val:.4f} ({loss.avg:.4f})\t'.format(loss=losses))


    def validate(self,):
        self.logger.info('begin test:')
        self.model.eval()
        mae1 = 0
        mae2 = 0
        with torch.no_grad():
            for img, density, dot_map in tqdm(self.val_loader):

                img = Variable(img).cuda()
                density = Variable(density).cuda()
                dot_map = Variable(dot_map).cuda()

                output1,output2 = self.model(img)
                output1=output1*(1.0/self.scale_times)
                output2=output2*(1.0/self.scale_times)

                diffs1=output1.data.sum(dim=[1,2,3])-density.data.sum(dim=[1,2,3])
                diffs2=output2.data.sum(dim=[1,2,3])-dot_map.data.sum(dim=[1,2,3])
                diffs1=diffs1.cpu().numpy()
                diffs2=diffs2.cpu().numpy()
                mae1 += sum(abs(diffs1))
                mae2 += sum(abs(diffs2))

        mae1 = mae1/(len(self.val_loader)*self.batch_size_test)
        mae2 = mae2/(len(self.val_loader)*self.batch_size_test)
        self.logger.info(' * MAE_1 {mae1:.3f} '.format(mae1=mae1))
        self.logger.info(' * MAE_2 {mae2:.3f} '.format(mae2=mae2))
        return (mae1+mae2)/2

    def resume(self):
        checkpoint = torch.load(self.resume_from)
        self.start_epoch = checkpoint['epoch']
        self.best_prec1 = checkpoint['best_prec1']
        self.model.load_state_dict(checkpoint['state_dict'])
        self.optimizer.load_state_dict(checkpoint['optimizer'])

        self.logger.info("=> loaded checkpoint '{}' (epoch {})".format(self.resume_from, checkpoint['epoch']))
    def load_checkpoint(self):
        checkpoint = torch.load(self.load_from)
        self.best_prec1 = checkpoint['best_prec1']
        self.model.load_state_dict(checkpoint['state_dict'])
        self.optimizer.load_state_dict(checkpoint['optimizer'])
        self.logger.info("=> loaded checkpoint '{}'".format(self.load_from))

    def adjust_learning_rate(self,epoch):
        epochs_drop = self.epochs_drop
        self.lr = self.lr * math.pow(self.drop_times, math.floor(epoch / epochs_drop))
        for param_group in self.optimizer.param_groups:
            param_group['lr'] = self.lr

    def run(self):
        if self.resume_from =='' and self.auto_resume==True:
            resume_from = find_latest_checkpoint(self.work_dir)
            if resume_from is not None:
                self.resume_from = resume_from
        if self.resume_from!='':
            self.resume()
        elif self.load_from!='':
            self.load_checkpoint()
        for epoch in range(self.start_epoch, self.epochs):
            self.adjust_learning_rate(epoch)

            self.train(epoch)
            prec1 = self.validate()
            is_best = prec1 < self.best_prec1
            self.best_prec1 = min(prec1, self.best_prec1)
            self.logger.info(' * best MAE {mae:.3f} '.format(mae=self.best_prec1))

            save_checkpoint({
                'epoch': epoch + 1,
                'state_dict': self.model.state_dict(),
                'best_prec1': self.best_prec1,
                'optimizer' : self.optimizer.state_dict(),
            }, is_best=is_best,saving_dir= self.work_dir,saving_prefix=self.cfg.timestamp)

