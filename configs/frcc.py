train_path='/home/houyi/datasets/CrowdX_Video/train_data/ADJ3_ZOOM0.3_DR8_AUG18'
test_path='/home/houyi/datasets/CrowdX_Video/test_data/ADJ3_ZOOM0.3_DR8'

batch_size_train=4
batch_size_test=2
scale_times=640
lr=1e-5

epochs_drop=5
drop_times=0.5

resume_from=''
load_from=''

auto_resume=True

workers=10
momentum=0.95
weight_decay=5*1e-4
start_epoch=0
epochs=100
seed=1000