from utils.logger import get_root_logger
from utils.seed import set_random_seed
from mmcv.utils import Config

import os
import mmcv
import time
from trainer.frcc_base_trainer import FRCC_Trainer


def env_setup(config_file):
    cfg = Config.fromfile(config_file)
    cfg.work_dir = os.path.join('./work_dirs',os.path.splitext(os.path.basename(config_file))[0])
    mmcv.mkdir_or_exist(os.path.abspath(cfg.work_dir))
    cfg.dump(os.path.join(cfg.work_dir, os.path.basename(config_file)))
    timestamp = time.strftime('%Y%m%d_%H%M%S', time.localtime())
    cfg.timestamp=timestamp
    log_file = os.path.join(cfg.work_dir, f'{timestamp}.log')
    logger = get_root_logger(log_file=log_file)
    set_random_seed(cfg.seed)
    logger.info(f'Config:\n{cfg.pretty_text}')
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    return cfg

if __name__ == '__main__':
    cfg=env_setup()

    trainer=FRCC_Trainer(cfg)
    trainer.run()















