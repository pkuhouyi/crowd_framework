import torch.nn as nn
from torchvision import models

class ResidualBlock(nn.Module):
    def __init__(self, input_channels, output_channels, stride=1):
        super(ResidualBlock, self).__init__()
        self.input_channels = input_channels
        self.output_channels = output_channels
        self.stride = stride
        self.bn1 = nn.BatchNorm2d(input_channels)
        self.relu = nn.ReLU(inplace=True)
        self.conv1 = nn.Conv2d(input_channels, output_channels//4, 1, 1, bias = False)
        self.bn2 = nn.BatchNorm2d(output_channels//4)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = nn.Conv2d(output_channels//4, output_channels//4, 3, stride, padding = 1, bias = False)
        self.bn3 = nn.BatchNorm2d(output_channels//4)
        self.relu = nn.ReLU(inplace=True)
        self.conv3 = nn.Conv2d(output_channels//4, output_channels, 1, 1, bias = False)
        self.conv4 = nn.Conv2d(input_channels, output_channels , 1, stride, bias = False)

    def forward(self, x):
        residual = x
        out = self.bn1(x)
        out1 = self.relu(out)
        out = self.conv1(out1)
        out = self.bn2(out)
        out = self.relu(out)
        out = self.conv2(out)
        out = self.bn3(out)
        out = self.relu(out)
        out = self.conv3(out)
        if (self.input_channels != self.output_channels) or (self.stride != 1):
            residual = self.conv4(out1)
        out += residual
        return out


class AttentionModule(nn.Module):

    def __init__(self, in_channels, out_channels):
        super(AttentionModule, self).__init__()

        self.trunk_branches = nn.Sequential(
            ResidualBlock(in_channels, out_channels),
            ResidualBlock(out_channels, out_channels)
        )
        self.mpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.softmax_blocks = ResidualBlock(in_channels, out_channels)
        self.interpolation = nn.UpsamplingBilinear2d(scale_factor=2)
        self.softmax2_blocks = nn.Sequential(
            nn.Conv2d(out_channels, out_channels , kernel_size = 1, stride = 1, bias = False),
            nn.Sigmoid()
        )

    def forward(self, x):
        out_trunk = self.trunk_branches(x)
        out_mpool = self.mpool(x)
        out_softmax = self.softmax_blocks(out_mpool)
        out_interp = self.interpolation(out_softmax)
        out_softmax = self.softmax2_blocks(out_interp)
        out = (1 + out_softmax) * out_trunk

        return out

class CSRNet(nn.Module):
    def __init__(self, load_weights=False):
        super(CSRNet, self).__init__()
        self.frontend_feat = [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512]
        self.frontend = make_layers(self.frontend_feat)


        self.AttentionModule_1=AttentionModule(512,256)
        self.AttentionModule_2=AttentionModule(256,256)

        self.backend_feat_den = [256, 128]
        self.backend_feat_dot = [256, 128]

        self.backend_den = make_layers(self.backend_feat_den,in_channels = 256, dilation = True)
        self.backend_dot = make_layers(self.backend_feat_dot,in_channels = 256, dilation = False)
        self.output_den_map = nn.Conv2d(128, 1, kernel_size=1)
        self.output_dot_map = nn.Conv2d(128, 1, kernel_size=1)

        if not load_weights:
            mod = models.vgg16(pretrained = True)
            self._initialize_weights()
            self.frontend.load_state_dict(mod.features[0:23].state_dict())
    def forward(self,x):
        x = self.frontend(x)

        x = self.AttentionModule_1(x)
        x = self.AttentionModule_2(x)
        y=x
        x = self.backend_den(x)
        x = self.output_den_map(x)

        y = self.backend_dot(y)
        y = self.output_dot_map(y)

        return [x,y]

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.xavier_normal_(m.weight,)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

def make_layers(cfg, in_channels = 3, batch_norm=False, dilation = False):
    if dilation:
        d_rate = 2
    else:
        d_rate = 1
    layers = []
    for v in cfg:
        if v == 'M':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        else:
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=d_rate,dilation = d_rate)
            if batch_norm:
                layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
            else:
                layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = v
    return nn.Sequential(*layers)                
