import unittest
import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import models
from torchvision.models import vgg16


class ResidualBlock(nn.Module):
    def __init__(self, input_channels, output_channels, stride=1):
        super(ResidualBlock, self).__init__()
        self.input_channels = input_channels
        self.output_channels = output_channels
        self.stride = stride
        self.bn1 = nn.BatchNorm2d(input_channels)
        self.relu = nn.ReLU(inplace=True)
        self.conv1 = nn.Conv2d(input_channels, output_channels//4, 1, 1, bias = False)
        self.bn2 = nn.BatchNorm2d(output_channels//4)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = nn.Conv2d(output_channels//4, output_channels//4, 3, stride, padding = 1, bias = False)
        self.bn3 = nn.BatchNorm2d(output_channels//4)
        self.relu = nn.ReLU(inplace=True)
        self.conv3 = nn.Conv2d(output_channels//4, output_channels, 1, 1, bias = False)
        self.conv4 = nn.Conv2d(input_channels, output_channels , 1, stride, bias = False)

    def forward(self, x):
        residual = x
        out = self.bn1(x)
        out1 = self.relu(out)
        out = self.conv1(out1)
        out = self.bn2(out)
        out = self.relu(out)
        out = self.conv2(out)
        out = self.bn3(out)
        out = self.relu(out)
        out = self.conv3(out)
        if (self.input_channels != self.output_channels) or (self.stride != 1):
            residual = self.conv4(out1)
        out += residual
        return out


class AttentionModule(nn.Module):

    def __init__(self, in_channels, out_channels):
        super(AttentionModule, self).__init__()

        self.trunk_branches = nn.Sequential(
            ResidualBlock(in_channels, out_channels),
            ResidualBlock(out_channels, out_channels)
        )
        self.mpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.softmax_blocks = ResidualBlock(in_channels, out_channels)
        self.interpolation = nn.UpsamplingBilinear2d(scale_factor=2)
        self.softmax2_blocks = nn.Sequential(
            nn.Conv2d(out_channels, out_channels , kernel_size = 1, stride = 1, bias = False),
            nn.Sigmoid()
        )

    def forward(self, x):
        out_trunk = self.trunk_branches(x)
        out_mpool = self.mpool(x)
        out_softmax = self.softmax_blocks(out_mpool)
        out_interp = self.interpolation(out_softmax)
        out_softmax = self.softmax2_blocks(out_interp)
        out = (1 + out_softmax) * out_trunk

        return out




class ResBlock(nn.Module):
    def __init__(self, conv_dim):
        super(ResBlock, self).__init__()
        self.conv1 = nn.Conv2d(in_channels=conv_dim, out_channels=conv_dim,
                               kernel_size=3, stride=1, padding=1)
        self.conv2 = nn.Conv2d(in_channels=conv_dim, out_channels=conv_dim,
                               kernel_size=3, stride=1, padding=1)

    def forward(self, input):
        out = self.conv1(input)
        out = F.relu(out)
        out = self.conv2(out)
        out = input + out
        return out


class ConvLeaky(nn.Module):
    def __init__(self, in_dim, out_dim):
        super(ConvLeaky, self).__init__()
        self.conv1 = nn.Conv2d(in_channels=in_dim, out_channels=out_dim,
                               kernel_size=3, stride=1, padding=1)
        self.conv2 = nn.Conv2d(in_channels=out_dim, out_channels=out_dim,
                               kernel_size=3, stride=1, padding=1)

    def forward(self, input):
        out = self.conv1(input)
        out = F.leaky_relu(out, 0.2)
        out = self.conv2(out)
        out = F.leaky_relu(out, 0.2)
        return out


class FNetBlock(nn.Module):
    def __init__(self, in_dim, out_dim, typ):
        super(FNetBlock, self).__init__()
        self.convleaky = ConvLeaky(in_dim, out_dim)
        if typ == "maxpool":
            self.final = lambda x: F.max_pool2d(x, kernel_size=2)
        elif typ == "bilinear":
            self.final = lambda x: F.interpolate(x, scale_factor=2, mode="bilinear")
        else:
            raise Exception('Type does not match any of maxpool or bilinear')

    def forward(self, input):
        out = self.convleaky(input)
        out = self.final(out)
        return out



class FNet(nn.Module):
    def __init__(self, in_dim=6):
        super(FNet, self).__init__()
        self.convPool1 = FNetBlock(in_dim, 32, typ="maxpool")
        self.convPool2 = FNetBlock(32, 64, typ="maxpool")
        self.convPool3 = FNetBlock(64, 128, typ="maxpool")
        self.convBinl1 = FNetBlock(128, 256, typ="bilinear")
        self.convBinl2 = FNetBlock(256, 128, typ="bilinear")
        self.convBinl3 = FNetBlock(128, 64, typ="bilinear")
        self.seq = nn.Sequential(self.convPool1, self.convPool2, self.convPool3,
                                 self.convBinl1, self.convBinl2, self.convBinl3)
        self.conv1 = nn.Conv2d(in_channels=64, out_channels=32, kernel_size=3, stride=1, padding=1)
        self.conv2 = nn.Conv2d(in_channels=32, out_channels=2, kernel_size=3, stride=1, padding=1)

    def forward(self, input):
        out = self.seq(input)
        out = self.conv1(out)
        out = F.leaky_relu(out, 0.2)
        out = self.conv2(out)
        self.out = torch.tanh(out)
        return self.out

def make_layers(cfg, in_channels = 3, batch_norm=False, dilation = False):
    if dilation:
        d_rate = 2
    else:
        d_rate = 1
    layers = []
    for v in cfg:
        if v == 'M':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        else:
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=d_rate,dilation = d_rate)
            if batch_norm:
                layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
            else:
                layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = v
    return nn.Sequential(*layers)


class SCCNet(nn.Module):
    def __init__(self, load_weights=False):
        super(SCCNet, self).__init__()
        self.frontend_feat = [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512]
        self.frontend = make_layers(self.frontend_feat)


        self.AttentionModule_1=AttentionModule(512,256)
        self.AttentionModule_2=AttentionModule(256,256)

        self.backend_feat_den = [256, 128]
        self.backend_feat_dot = [256, 128]

        self.backend_den = make_layers(self.backend_feat_den,in_channels = 256, dilation = True)
        self.output_den_map = nn.Conv2d(128, 1, kernel_size=1)

        if not load_weights:
            mod = models.vgg16(pretrained = True)
            self._initialize_weights()
            self.frontend.load_state_dict(mod.features[0:23].state_dict())
    def forward(self,x):
        x = self.frontend(x)

        x = self.AttentionModule_1(x)
        x = self.AttentionModule_2(x)

        x = self.backend_den(x)
        x = self.output_den_map(x)
        return x

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.xavier_normal_(m.weight,)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)


class FusionNet(nn.Module):
    def __init__(self, ):
        super(FusionNet, self).__init__()
        self.inputConv = nn.Conv2d(in_channels=2, out_channels=4, kernel_size=3, stride=1, padding=1)
        self.ResBlocks = nn.Sequential(*[ResBlock(4) for i in range(10)])
        self.outputConv = nn.Conv2d(4, 1, kernel_size=1)

    def forward(self, input):
        out = self.inputConv(input)
        out = self.ResBlocks(out)
        out = self.outputConv(out)
        return out


class FRCC(nn.Module):
    def __init__(self, device):
        super(FRCC, self).__init__()

        self.device=device
        #网络
        self.fnet = FNet()
        self.sccnet = SCCNet()
        self.density_fusion_net=FusionNet()

    def init_hidden(self,batch_size, img_height, img_width):
        #光流的归一化相关操作
        self.img_width = img_width
        self.img_height = img_height
        self.batch_size = batch_size
        height_gap = 2.0 / (self.img_height - 1)
        width_gap = 2.0 / (self.img_width - 1)
        height, width = torch.meshgrid([torch.range(-1, 1, height_gap), torch.range(-1, 1, width_gap)])
        self.crowdImg_identity = torch.stack([width, height]).to(self.device)

        self.preCrowdImg = torch.zeros([self.batch_size, 3, self.img_height, self.img_width]).to(self.device)
        self.EstDensityMap = torch.zeros([self.batch_size, 1, self.img_height//8, self.img_width//8]).to(self.device)


    def forward(self, input):
        preflow = torch.cat((input, self.preCrowdImg), dim=1)
        flow = self.fnet(preflow)
        relative_place = flow + self.crowdImg_identity
        self.Img_afterWarp = F.grid_sample(self.preCrowdImg, relative_place.permute(0, 2, 3, 1))

        relative_place_anno = F.interpolate(relative_place, scale_factor=0.125, mode="bilinear")
        density_afterWarp = F.grid_sample(self.EstDensityMap, relative_place_anno.permute(0, 2, 3, 1))

        #单人群计数网络
        _EstDensityMap_Single = self.sccnet(input)

        #融合网络
        density_Input = torch.cat((_EstDensityMap_Single, density_afterWarp), dim=1)

        EstDensityMap=self.density_fusion_net(density_Input)

        #赋值前一个变量
        self.preCrowdImg = input
        self.EstDensityMap = EstDensityMap

        return self.EstDensityMap, self.Img_afterWarp


#用来做正则的，鼓励图像更加平滑
class TVLoss(nn.Module):
    def __init__(self, tv_loss_weight=1):
        super(TVLoss, self).__init__()
        self.tv_loss_weight = tv_loss_weight

    def forward(self, x):
        batch_size = x.size()[0]
        h_x = x.size()[2]
        w_x = x.size()[3]
        count_h = self.tensor_size(x[:, :, 1:, :])
        count_w = self.tensor_size(x[:, :, :, 1:])
        h_tv = torch.pow((x[:, :, 1:, :] - x[:, :, :h_x - 1, :]), 2).sum()
        w_tv = torch.pow((x[:, :, :, 1:] - x[:, :, :, :w_x - 1]), 2).sum()
        return self.tv_loss_weight * 2 * (h_tv / count_h + w_tv / count_w) / batch_size

    @staticmethod
    def tensor_size(t):
        return t.size()[1] * t.size()[2] * t.size()[3]


class GeneratorLoss(nn.Module):
    def __init__(self):
        super(GeneratorLoss, self).__init__()
        vgg = vgg16(pretrained=True)
        loss_network = nn.Sequential(*list(vgg.features)[:31]).eval()
        for param in loss_network.parameters():
            param.requires_grad = False
        self.loss_network = loss_network
        self.mse_loss = nn.MSELoss()
        self.tv_loss = TVLoss()

    def forward(self,est_density_map, density_map_gt, crowd_est_cur, crowd_img_cur, idx):

        loss_density = self.mse_loss(est_density_map, density_map_gt)
        tv_loss_density = self.tv_loss(est_density_map)
        # flow loss
        if idx != 0:
            flow_loss = self.mse_loss(crowd_est_cur, crowd_img_cur)
        else:
            flow_loss = 0
        base_loss=loss_density
        return base_loss +2e-8 * tv_loss_density + 0.0001 * flow_loss
